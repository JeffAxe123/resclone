package io.gitlab.jfronny.resclone.io;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.function.Predicate;

public class PathPruneVisitor extends SimpleFileVisitor<Path> {
    Predicate<Path> removalSelector;
    public PathPruneVisitor(Predicate<Path> removalSelector) {
        this.removalSelector = removalSelector;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (removalSelector.test(file)) Files.delete(file);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        if (removalSelector.test(dir)) Files.walkFileTree(dir, new RemoveDirVisitor());
        return super.postVisitDirectory(dir, exc);
    }
}
