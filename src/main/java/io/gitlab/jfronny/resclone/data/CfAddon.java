package io.gitlab.jfronny.resclone.data;

import java.util.Set;

public class CfAddon {
    public String downloadUrl;
    public String fileDate;
    public Set<String> gameVersion;
}
