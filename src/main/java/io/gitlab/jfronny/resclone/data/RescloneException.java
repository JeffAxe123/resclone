package io.gitlab.jfronny.resclone.data;

public class RescloneException extends Exception {
    public RescloneException(String message) {
        super(message);
    }

    public RescloneException(String message, Throwable cause) {
        super(message, cause);
    }
}
