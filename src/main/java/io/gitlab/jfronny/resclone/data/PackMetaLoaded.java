package io.gitlab.jfronny.resclone.data;

import java.nio.file.Path;

public class PackMetaLoaded {
    public Path zipPath;
    public String name;

    public PackMetaLoaded(Path zipPath, String name) {
        this.zipPath = zipPath;
        this.name = name;
    }
}
