package io.gitlab.jfronny.resclone.data;

public class PackMetaUnloaded {
    public final String fetcher;
    public final String source;
    public final String name;
    public final boolean forceDownload;

    public PackMetaUnloaded(String fetcher, String source, String name, boolean forceDownload) {
        this.fetcher = fetcher;
        this.source = source;
        this.name = name;
        this.forceDownload = forceDownload;
    }
}
