package io.gitlab.jfronny.resclone;

import net.fabricmc.fabric.api.resource.ModResourcePack;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.metadata.ModMetadata;
import net.minecraft.resource.ZipResourcePack;

import java.io.File;

public class RescloneResourcePack extends ZipResourcePack implements ModResourcePack {
    private final String name;
    public RescloneResourcePack(File file, String name) {
        super(file);
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public ModMetadata getFabricModMetadata() {
        return FabricLoader.getInstance().getModContainer(Resclone.MOD_ID).get().getMetadata();
    }
}
