package io.gitlab.jfronny.resclone.config;

import com.google.gson.reflect.TypeToken;
import io.gitlab.jfronny.resclone.Resclone;
import io.gitlab.jfronny.resclone.api.RescloneApi;
import io.gitlab.jfronny.resclone.data.PackMetaUnloaded;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;

public class ConfigLoader {
    public static void load(RescloneApi api) {
        Path configPath = api.getConfigPath().resolve("config.json");
        if (!Files.exists(configPath)) {
            save(api, new HashSet<>());
        }
        try {
            StringBuilder text = new StringBuilder();
            for (String s : Files.readAllLines(configPath)) {
                text.append("\r\n");
                text.append(s);
            }
            Set<PackMetaUnloaded> data = Resclone.gson.fromJson(text.toString(), new TypeToken<Set<PackMetaUnloaded>>(){}.getType());
            for (PackMetaUnloaded meta : data) {
                api.addPack(meta.fetcher, meta.source, meta.name, meta.forceDownload);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void save(RescloneApi api, Set<PackMetaUnloaded> data) {
        Path configPath = api.getConfigPath().resolve("config.json");
        Set<String> text = new HashSet<>();
        text.add(Resclone.gson.toJson(data));
        try {
            Files.write(configPath, text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
