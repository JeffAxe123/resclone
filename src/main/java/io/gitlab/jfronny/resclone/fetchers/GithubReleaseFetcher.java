package io.gitlab.jfronny.resclone.fetchers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.gitlab.jfronny.resclone.data.RescloneException;

public class GithubReleaseFetcher extends PackFetcher {
    @Override
    public String getSourceTypeName() {
        return "github-release";
    }

    @Override
    String getDownloadUrl(String baseUrl) throws RescloneException {
        String[] parts = baseUrl.split("/");
        if (parts.length == 2) {
            try {
                JsonObject latestRelease = readJsonFromURL("https://api.github.com/repos/" + parts[0] + "/" + parts[1] + "/releases/latest", JsonObject.class);
                String res = null;
                for (JsonElement element : latestRelease.get("assets").getAsJsonArray()) {
                    JsonObject o = element.getAsJsonObject();
                    if ("application/x-zip-compressed".equals(o.get("content_type").getAsString())
                            || o.get("name").getAsString().endsWith(".zip")) {
                        res = o.get("browser_download_url").getAsString();
                        break;
                    }
                }
                if (res == null) {
                    System.out.println("Could not find release asset for " + baseUrl + ", using zipball");
                    return latestRelease.get("zipball_url").getAsString();
                }
                return res;
            } catch (Throwable e) {
                throw new RescloneException("Failed to get github release asset", e);
            }
        }
        else {
            throw new RescloneException("Format for github-release is USER/REPO");
        }
    }
}
