package io.gitlab.jfronny.resclone.fetchers;

public class BasicFileFetcher extends PackFetcher {
    @Override
    public String getSourceTypeName() {
        return "file";
    }

    @Override
    public String getDownloadUrl(String baseUrl) {
        return baseUrl;
    }
}
