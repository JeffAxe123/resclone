package io.gitlab.jfronny.resclone.fetchers;

import io.gitlab.jfronny.resclone.data.CfAddon;
import io.gitlab.jfronny.resclone.data.RescloneException;
import net.minecraft.MinecraftVersion;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CurseforgeFetcher extends PackFetcher {
    @Override
    public String getSourceTypeName() {
        return "curseforge";
    }

    @Override
    public String getDownloadUrl(String baseUrl) throws RescloneException {
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");

            String version = MinecraftVersion.field_25319.getName();

            CfAddon latest = null;
            Date latestDate = null;
            boolean foundMatchingVersion = false;

            for (CfAddon addon : readJsonFromURLSet("https://addons-ecs.forgesvc.net/api/v2/addon/" + baseUrl + "/files", CfAddon.class)) {
                Date d = df.parse(addon.fileDate);
                if (foundMatchingVersion && !addon.gameVersion.contains(version))
                    continue;
                if (!foundMatchingVersion && addon.gameVersion.contains(version)) {
                    foundMatchingVersion = true;
                    latest = null;
                }
                if (latest == null || d.after(latestDate)) {
                    latest = addon;
                    latestDate = d;
                }
            }

            if (!foundMatchingVersion)
                System.err.println("Could not find pack for matching version, using latest");
            return latest.downloadUrl;
        } catch (Throwable e) {
            throw new RescloneException("Could not get CF download for " + baseUrl, e);
        }
    }
}
