package io.gitlab.jfronny.resclone.fetchers;

import io.gitlab.jfronny.resclone.data.RescloneException;

public class GithubMasterFetcher extends PackFetcher {
    @Override
    public String getSourceTypeName() {
        return "github-master";
    }

    @Override
    public String getDownloadUrl(String baseUrl) throws RescloneException {
        String[] parts = baseUrl.split("/");
        String url = "";
        if (parts.length == 2) {
            url = getStr(parts[0], parts[1], "main");
            if (!urlValid(url))
                url = getStr(parts[0], parts[1], "master");
        }
        else if (parts.length == 3) {
            url = getStr(parts[0], parts[1], parts[2]);
        }
        else {
            throw new RescloneException("Format for github-master is USER/REPO[/BRANCH]");
        }
        return url;
    }

    private String getStr(String user, String repo, String branch) {
        return "https://github.com/" + user + "/" + repo + "/archive/" + branch + ".zip";
    }
}
