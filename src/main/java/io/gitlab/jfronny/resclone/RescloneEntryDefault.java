package io.gitlab.jfronny.resclone;

import io.gitlab.jfronny.resclone.api.RescloneApi;
import io.gitlab.jfronny.resclone.api.RescloneEntry;
import io.gitlab.jfronny.resclone.config.ConfigLoader;
import io.gitlab.jfronny.resclone.fetchers.BasicFileFetcher;
import io.gitlab.jfronny.resclone.fetchers.CurseforgeFetcher;
import io.gitlab.jfronny.resclone.fetchers.GithubMasterFetcher;
import io.gitlab.jfronny.resclone.fetchers.GithubReleaseFetcher;
import io.gitlab.jfronny.resclone.processors.PruneVanillaProcessor;
import net.fabricmc.api.EnvType;
import net.fabricmc.loader.api.FabricLoader;

public class RescloneEntryDefault implements RescloneEntry {
    @Override
    public void init(RescloneApi api) {
        api.addFetcher(new BasicFileFetcher());
        api.addFetcher(new GithubMasterFetcher());
        api.addFetcher(new GithubReleaseFetcher());
        api.addFetcher(new CurseforgeFetcher());
        if (FabricLoader.getInstance().getEnvironmentType() == EnvType.CLIENT)
            api.addProcessor(new PruneVanillaProcessor());
        ConfigLoader.load(api);
    }
}
