package io.gitlab.jfronny.resclone.processors;

import io.gitlab.jfronny.resclone.data.RescloneException;
import io.gitlab.jfronny.resclone.io.PathPruneVisitor;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;

public class RemoveEmptyProcessor extends PackProcessor {
    @Override
    public void process(FileSystem p) throws RescloneException {
        if (Files.exists(p.getPath("/assets"))) {
            try {
                Files.walkFileTree(p.getPath("/assets"), new PathPruneVisitor(s -> {
                    if (Files.isDirectory(s)) {
                        try {
                            return !Files.newDirectoryStream(s).iterator().hasNext();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    return false;
                }));
            } catch (Throwable e) {
                throw new RescloneException("Failed to prune empty directories", e);
            }
        }
    }
}
