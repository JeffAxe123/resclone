package io.gitlab.jfronny.resclone.processors;

import io.gitlab.jfronny.resclone.io.MoveDirVisitor;
import io.gitlab.jfronny.resclone.data.RescloneException;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class RootPathProcessor extends PackProcessor {
    @Override
    public void process(FileSystem p) throws RescloneException {
        if (!Files.exists(p.getPath("/pack.mcmeta"))) {
            try {
                Path root = p.getPath("/");
                for (Path path : Files.newDirectoryStream(root)) {
                    if (Files.isDirectory(path) && Files.exists(path.resolve("pack.mcmeta"))) {
                        Files.walkFileTree(path, new MoveDirVisitor(path, root, StandardCopyOption.REPLACE_EXISTING));
                    }
                }
            } catch (IOException e) {
                throw new RescloneException("Could not fix root path", e);
            }
        }
    }
}
