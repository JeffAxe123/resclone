package io.gitlab.jfronny.resclone.processors;

import io.gitlab.jfronny.resclone.data.RescloneException;
import io.gitlab.jfronny.resclone.io.PathPruneVisitor;
import net.minecraft.client.MinecraftClient;
import net.minecraft.resource.ResourcePack;
import net.minecraft.resource.ResourcePackManager;
import net.minecraft.resource.ResourcePackProfile;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

public class PruneVanillaProcessor extends PackProcessor {
    @Override
    public void process(FileSystem p) throws RescloneException {
        ClassLoader cl = MinecraftClient.class.getClassLoader();
        try {
            if (Files.isDirectory(p.getPath("/assets/minecraft"))) {
                Files.walkFileTree(p.getPath("/assets/minecraft"), new PathPruneVisitor((s) -> {
                    if (Files.isDirectory(s))
                        return false;
                    try {
                        InputStream vn = cl.getResourceAsStream(p.getPath("/").relativize(s).toString());
                        if (vn != null) {
                            InputStream pk = Files.newInputStream(s, StandardOpenOption.READ);
                            return IOUtils.contentEquals(vn, pk);
                        }
                    }
                    catch (Throwable e) {
                        e.printStackTrace();
                    }
                    return false;
                }));
            }
        } catch (IOException e) {
            throw new RescloneException("Could not prune vanilla files", e);
        }
    }
}
