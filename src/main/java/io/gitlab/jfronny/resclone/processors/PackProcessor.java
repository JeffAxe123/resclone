package io.gitlab.jfronny.resclone.processors;

import io.gitlab.jfronny.resclone.data.RescloneException;

import java.nio.file.FileSystem;

public abstract class PackProcessor {
    public abstract void process(FileSystem p) throws RescloneException;
}
