package io.gitlab.jfronny.resclone.api;

import io.gitlab.jfronny.resclone.fetchers.PackFetcher;
import io.gitlab.jfronny.resclone.processors.PackProcessor;

import java.nio.file.Path;

public interface RescloneApi {
    void addFetcher(PackFetcher fetcher);
    void addProcessor(PackProcessor processor);
    void addPack(String fetcher, String pack, String name);
    void addPack(String fetcher, String pack, String name, boolean forceRedownload);
    void reload();
    Path getConfigPath();
}
