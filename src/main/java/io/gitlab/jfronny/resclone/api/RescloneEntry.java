package io.gitlab.jfronny.resclone.api;

import io.gitlab.jfronny.resclone.data.RescloneException;

public interface RescloneEntry {
    void init(RescloneApi api) throws RescloneException;
}
