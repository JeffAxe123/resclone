package io.gitlab.jfronny.resclone;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

public class PackUrlCache {
    Properties properties = new Properties();
    private Path file;

    public PackUrlCache(Path file) {
        this.file = file;
        if (Files.exists(file)) {
            try (BufferedReader r = Files.newBufferedReader(file)) {
                properties.load(r);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void save() {
        try (BufferedWriter w = Files.newBufferedWriter(file)) {
            properties.store(w, "This is an internal file used for offline pack loading, do not edit");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean containsKey(String key) {
        return properties.containsKey(key);
    }

    public String get(String key) {
        return properties.getProperty(key);
    }

    public void set(String key, String value) {
        properties.setProperty(key, value);
    }
}
