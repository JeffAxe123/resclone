package io.gitlab.jfronny.resclone.mixin;

import io.gitlab.jfronny.resclone.Resclone;
import io.gitlab.jfronny.resclone.RescloneResourcePack;
import io.gitlab.jfronny.resclone.data.PackMetaLoaded;
import net.minecraft.resource.*;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.function.Consumer;

@Mixin(FileResourcePackProvider.class)
public class FileResourcePackProviderMixin {
    @Shadow @Final private ResourcePackSource field_25345;

    @Inject(at = @At("TAIL"), method = "register(Ljava/util/function/Consumer;Lnet/minecraft/resource/ResourcePackProfile$Factory;)V")
    public void registerExtra(Consumer<ResourcePackProfile> consumer, ResourcePackProfile.Factory factory, CallbackInfo info) {
        for (PackMetaLoaded meta : Resclone.downloadedPacks) {
            ResourcePackProfile resourcePackProfile = ResourcePackProfile.of(
                    "resclone/" + meta.name,
                    false,
                    () -> new RescloneResourcePack(meta.zipPath.toFile(), meta.name),
                    factory,
                    ResourcePackProfile.InsertionPosition.TOP,
                    this.field_25345
            );
            if (resourcePackProfile != null) {
                consumer.accept(resourcePackProfile);
            }
        }
    }
}
